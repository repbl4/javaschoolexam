package com.tsystems.javaschool.tasks.subsequence;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        Map<Integer, Object> map = new LinkedHashMap<>();
        int count = 0;
        boolean sorted = true;

        try {
            int sizeX = x.size();
            int sizeY = y.size();
            for (int i = 0; i < sizeX; i++) {
                for (int j = 0; j < sizeY; j++) {
                    if (x.get(i).equals(y.get(j))) {
                        count += 1;
                        map.put(j, y.get(j));
                        break;
                    }
                }
            }
            sorted = count == sizeX;

            Object[] keySet;
            keySet = map.keySet().toArray();
            for (int i = 1; i < keySet.length; i++) {
                if ((int) keySet[i - 1] > (int) keySet[i]) {
                    sorted = false;
                    break;
                }
            }
            return sorted;
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }
}
