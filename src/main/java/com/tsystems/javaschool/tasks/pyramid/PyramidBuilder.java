package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        try {

            Collections.sort(inputNumbers);
        } catch (Exception e) {
            throw new CannotBuildPyramidException();
        } catch (Error e) {
            throw new CannotBuildPyramidException();
        }
        List<List<Integer>> pyramid = new ArrayList<>();
        int column = 1;
        int indexOfList = 0;
        for (int string = 0; ; string++) {
            pyramid.add(new ArrayList<>());
            try {
                for (int i = 0; i < column; i++) {
                    if (i % 2 != 1) {
                        pyramid.get(string).add(inputNumbers.get(indexOfList++));
                    } else {
                        pyramid.get(string).add(0);
                    }
                }
                if (indexOfList == inputNumbers.size()) {
                    zeros(pyramid, inputNumbers.size());
                    break;
                }
                column += 2;
            } catch (Exception e) {
                throw new CannotBuildPyramidException();
            }

        }
        int length = pyramid.get(pyramid.size() - 1).size();
        int[][] array = new int[pyramid.size()][length];
        for (int i = 0; i < pyramid.size(); i++) {
            for (int j = 0; j < length; j++) {
                array[i][j] = pyramid.get(i).get(j);
            }
        }
        return array;
    }


    public void zeros(List<List<Integer>> pyramid, int arrayLength) {
        int length = pyramid.get(pyramid.size() - 1).size();
        for (int i = 0; i < pyramid.size(); i++) {
            for (int j = 0; j < arrayLength; j++) {
                if (pyramid.get(i).size() < arrayLength) {
                    int size = pyramid.get(i).size();
                    for (int k = 0; k < (length - size) / 2; k++) {
                        pyramid.get(i).add(0, 0);
                    }
                    for (int k = 0; k < (length - size) / 2; k++) {
                        pyramid.get(i).add(pyramid.get(i).size(), 0);
                    }
                }
            }
        }
    }


}
