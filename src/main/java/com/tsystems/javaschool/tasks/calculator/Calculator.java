package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.EmptyStackException;
import java.util.Locale;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        String parseExpression;
        try {
            parseExpression = parse(filter(statement));
        } catch (Exception e) {
            return null;
        }
        StringBuilder operand = new StringBuilder();
        Stack<Double> stack = new Stack<>();


        try {
            for (int i = 0; i < parseExpression.length(); i++) {
                if (parseExpression.charAt(i) == ' ') {
                    continue;
                }
                if (getPriority(parseExpression.charAt(i)) == 0) {
                    while (parseExpression.charAt(i) != ' ' &&
                            getPriority(parseExpression.charAt(i)) == 0) {
                        operand.append(parseExpression.charAt(i++));
                        if (i == parseExpression.length()) {
                            break;
                        }
                    }
                    stack.push(Double.parseDouble(operand.toString()));
                    operand = new StringBuilder();
                }
                if (getPriority(parseExpression.charAt(i)) > 1) {
                    double a = stack.pop(), b = stack.pop();
                    if (a == 0) {
                        return null;
                    }
                    switch (parseExpression.charAt(i)) {
                        case '+':
                            stack.push(b + a);
                            break;
                        case '-':
                            stack.push(b - a);
                            break;
                        case '*':
                            stack.push(b * a);
                            break;
                        case '/':
                            stack.push(b / a);
                            break;
                        default:
                            break;
                    }
                }

            }
            Double result = stack.pop();
            DecimalFormatSymbols dfs = new DecimalFormatSymbols(Locale.getDefault());
            dfs.setDecimalSeparator('.');
            DecimalFormat df = new DecimalFormat("#.####", dfs);
            return df.format(result);
        } catch (Exception e) {
            return null;
        }
    }


    private String parse(String expression) throws EmptyStackException {
        StringBuilder builder = new StringBuilder();
        Stack<Character> stack = new Stack<>();

        int priority;
        for (int i = 0; i < expression.length(); i++) {
            priority = getPriority(expression.charAt(i));
            if (priority == 0) {
                builder.append(expression.charAt(i));
            }
            if (priority == 1) {
                stack.push(expression.charAt(i));
            }
            if (priority > 1) {
                builder.append(' ');
                while (!stack.empty()) {
                    if (getPriority(stack.peek()) >= priority) {
                        builder.append(stack.pop());
                    } else break;
                }
                stack.push(expression.charAt(i));
            }
            if (priority == -1) {
                builder.append(' ');
                while (getPriority(stack.peek()) != 1) {
                    builder.append(stack.pop());
                }
                stack.pop();
            }
        }
        while (!stack.empty()) {
            builder.append(stack.pop());
        }
        return builder.toString();
    }

    private Integer getPriority(char symbol) {
        switch (symbol) {
            case '*':
            case '/':
                return 3;
            case '+':
            case '-':
                return 2;
            case '(':
                return 1;
            case ')':
                return -1;
            case ',':
                return null;
            default:
                return 0;
        }
    }

    private String filter(String expression) {
        String withoutSpaces = expression.replaceAll(" ", "");
        StringBuilder builder = new StringBuilder();
        if (expression.contains("..") ||
                expression.contains("++") || expression.contains("--")
                || expression.contains("//") || expression.contains("**")) {
            return null;
        }
        int openToken = 0;
        int closeToken = 0;
        for (char symbol : expression.toCharArray()) {
            if (symbol == '(') {
                openToken += 1;
            }
            if (symbol == ')') {
                closeToken += 1;
            }
        }
        if (openToken != closeToken) {
            return null;
        }
        for (int i = 0; i < withoutSpaces.length(); i++) {
            char element = withoutSpaces.charAt(i);
            if (element == '-') {
                if (i == 0) {
                    builder.append('0');
                } else if (withoutSpaces.charAt(i - 1) == '(') {
                    builder.append('0');
                }
            }
            builder.append(element);
        }
        return builder.toString();
    }

}
